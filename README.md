
# appveyor-lua [![Build status](https://ci.appveyor.com/api/projects/status/x1p2tfm0uwb8m82n?svg=true)](https://ci.appveyor.com/project/n1tehawk/appveyor-lua)


This is a simple 'framework' project that provides different Lua implementations
for Windows, on the Appveyor CI platform.

It comes with an "install step" batch file to set up the desired Lua version
depending on the `LUAENV` environment variable, which allows easy creation
of a build matrix (to test against multiple Lua versions).

Currently `LUAENV` supports the following choices:

* cinst
* lua51
* lua52
* lua53
* luajit20
* luajit21

`cinst` is the standard way to install Lua on Appveyor - it's basically just
requesting Chocolatey to retrieve the corresponding package (`cinst lua`).
Currently (as of 2016-03) that is Lua version 5.1.4.

`lua51` retrieves the Lua 5.1 interpreter from the sourceforge
[LuaBinaries](http://sourceforge.net/projects/luabinaries/files/)
project. This installs Lua 5.1.5.

`lua52` and `lua53` will do the same for Lua 5.2 / 5.3, with the current
versions being 5.2.4 and 5.3.2.

`luajit20` will do a minimalistic MinGW build straight from the
[LuaJIT sources](http://luajit.org/download.html). This is using the stable
v2.0 branch (currently LuaJIT-2.0.4).

`luajit21` is similar, but uses v2.1 instead (LuaJIT-2.1.0-beta2).

Each choice will then set the `LUA` environment variable, offering a uniform
way to invoke the installed Lua executable (e.g. `%LUA% -v`).

---
Note: I'm observing `lua51` to be consistently quicker than `cinst`, so maybe
that should be the preferred / "standard" choice for the Lua 5.1 interpreter.
The "cinst" target also may encounter problems sporadically, e.g. if Chocolatey
fails to contact the package servers. (For this reason the configuration file
allows a failure on the "cinst" build job.)
